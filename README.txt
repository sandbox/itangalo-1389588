The Word list module allows creating and handling a global word list on your
site. The words are exposed to Rules, allowing for things like blocking certain
words in comments.

Word list comes with a sample configuration bundled as a feature. It checks if
new comments contains any of the listed words, and if so unpublishes the comment
and displays an explaining message to the user.
